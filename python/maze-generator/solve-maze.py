import matplotlib.pyplot as plt
import matplotlib.colors as colors
import argparse
from pathlib import Path


def BFS(maze, src, dest):
    queue = []

    visited = {}
    distance = {}
    prev = {}
    for i in range(len(maze)):
        for j in range(len(maze[0])):
            visited[(i, j)] = False
            distance[(i, j)] = 100000
            prev[(i, j)] = -1

    visited[src] = True
    distance[src] = 0
    queue.append(src)

    while len(queue) > 0:
        (x, y) = queue.pop()
        # for adjacent nodes...
        neighbors = []
        if x > 0:
            neighbors.append((x-1, y))
        if x < len(maze)-1:
            neighbors.append((x+1, y))
        if y > 0:
            neighbors.append((x, y-1))
        if y < len(maze[0])-1:
            neighbors.append((x, y+1))

        for n in neighbors:
            if not visited[n] and maze[n[0]][n[1]] > 0:
                visited[n] = True
                distance[n] = distance[(x, y)] + 1
                prev[n] = (x, y)
                queue.append(n)

                if n == dest:
                    # empty queue to get out
                    queue = []

    path = []
    current = prev[dest]
    while prev[current] != -1:
        path.append(current)
        current = prev[current]

    return path


def main():
    parser = argparse.ArgumentParser(
        usage="python %(prog)s [OPTIONS] <filename>",
        description="Solves a maze from a txt file"
    )

    parser.add_argument("-o", "--output", action="store_true", default=False,
                        required=False,
                        help="if set, the solved maze is written to the disk")

    parser.add_argument("filename", help="the (txt) filename to solve")

    args = parser.parse_args()
    output = args.output
    filename = args.filename

    maze = []
    with open(filename) as f:
        for line in f:
            maze.append([int(i) for i in line if i != '\n'])

    # solve the maze?
    # find the start
    startx = -1
    starty = -1
    endx = -1
    endy = -1
    for x in range(len(maze)):
        for y in range(len(maze[0])):
            if maze[x][y] == 2:
                startx = x
                starty = y
            elif maze[x][y] == 3:
                endx = x
                endy = y

    # BFS starting at start
    path = BFS(maze, (startx, starty), (endx, endy))
    for (x, y) in path:
        maze[x][y] = 4

    n = (len(maze[0]) - 1) // 2
    cmap = colors.ListedColormap(["black", "white", "green", "red", "orange"])
    output_size = max(n // 2, 5)
    random_name = Path(filename).stem
    plt.figure(num=random_name+"-solved", figsize=(output_size, output_size))
    plt.pcolormesh(maze, cmap=cmap)
    plt.gca().set_aspect('equal')
    plt.gca().invert_yaxis()
    plt.axis('off')
    plt.box(False)

    if output:
        plt.savefig(random_name + "-solved.png", bbox_inches='tight')
        for row in maze:
            with open(random_name + "-solved.txt", "w") as f:
                for x in row:
                    f.write(str(x))
                f.write("\n")
    else:
        plt.show()


if __name__ == "__main__":
    main()
