import matplotlib.pyplot as plt
import matplotlib.colors as colors
import random
import argparse

# An implementation of a Lehmer RNG
# Used to generate random numbers for the edge weights of our graph
class LehmerRNG:
    def __init__(self, seed):
        self.seed = seed

    def next_int(self):
        self.seed = (self.seed * 48271 % 0x7fffffff)
        return self.seed

    def next_float(self):
        return self.next_int() / 0x7fffffff


# An implemtation of a Disjoint-set data structure
#   using rank-ordering and compression
# Used by Kruskal's algorithm
class DisjointSet:
    def __init__(self):
        self.rank = {}
        self.parent = {}

    def make_set(self, x):
        self.rank[x] = 1
        self.parent[x] = x

    def find(self, x):
        if self.parent[x] != x:
            # compress the tree
            self.parent[x] = self.find(self.parent[x])

        return self.parent[x]

    def union(self, x, y):
        xset = self.find(x)
        yset = self.find(y)

        if xset == yset:
            return

        if self.rank[xset] > self.rank[yset]:
            self.parent[yset] = xset
        elif self.rank[yset] > self.rank[xset]:
            self.parent[xset] = yset
        else:
            self.parent[yset] = xset
            self.rank[xset] += 1


# main code
def main():
    # setting up the program command line arguments
    parser = argparse.ArgumentParser(
        usage="python %(prog)s [OPTIONS]",
        description="Generates a maze by generating a grid of <size> by" +
        " <size> nodes, then generating random weighted edges between" +
        " adjacent nodes in the grid. Kruskal's algorithm is used to find" +
        " a minimal spanning tree (MST), which is then used to construct the" +
        " maze."
    )

    parser.add_argument("-z", "--seed", default="0", type=str,
                        required=False,
                        help="the hexadecimal seed to use for the maze. if a" +
                        " seed is not specified, a random seed will be used")

    parser.add_argument("-s", "--size", default=20, type=int,
                        required=False,
                        help="the size of the maze (default: %(default)s)")

    parser.add_argument("-o", "--output", action="store_true", default=False,
                        required=False,
                        help="if set, the maze is written to a pair of files")

    args = parser.parse_args()
    seed = int(args.seed, 16)
    if seed == 0:
        seed = random.randint(0, 0x7fffffff)
    n = args.size
    output = args.output

    seed_display_name = "{:08x}".format(seed)
    print("Generating maze with seed", seed_display_name)

    maze_size = n * 2 + 1

    # nodes is a list of the nodes in our lattice-graph
    nodes = [i for i in range(n * n)]

    # generate edges for the entire grid with random weights
    rng = LehmerRNG(seed)
    edges = []
    for node in nodes:
        # connect node to right if not on the edge
        if node % n < n - 1:
            edges.append((rng.next_float(), (node, node + 1)))
        # connect node to up if not on the edge
        if node // n < n - 1:
            edges.append((rng.next_float(), (node, node + n)))

    # Kruskal's Algorithm to find MST
    d = DisjointSet()
    for node in nodes:
        d.make_set(node)

    picked_edges = []
    edges.sort()
    for (w, (u, v)) in edges:
        if d.find(u) != d.find(v):
            d.union(u, v)
            picked_edges.append((u, v))

    # default a maze of walls :)
    maze = [[0] * maze_size for _ in range(maze_size)]

    # fill in each node from our lattice-graph as empty
    for node in nodes:
        ux = (node // n * 2) + 1
        uy = (node % n * 2) + 1

        maze[uy][ux] = 1

    # fill in all the edges in our MST as empty
    for (u, v) in picked_edges:
        ux = (u // n * 2) + 1
        uy = (u % n * 2) + 1

        vx = (v // n * 2) + 1
        vy = (v % n * 2) + 1

        maze[(ux + vx) // 2][(uy + vy) // 2] = 1

    # entrance
    maze[-2][1] = 2
    # exit
    maze[1][-2] = 3

    # draw the maze using matplotlib
    cmap = colors.ListedColormap(["black", "white", "green", "red"])
    output_size = max(n // 2, 5)
    plt.figure(num=seed_display_name, figsize=(output_size, output_size))
    plt.pcolormesh(maze, cmap=cmap)
    plt.gca().set_aspect('equal')
    plt.gca().invert_yaxis()
    plt.axis('off')
    plt.box(False)

    if output:
        # save to an image
        plt.savefig(seed_display_name + ".png", bbox_inches='tight')

        # save to txt file
        with open(seed_display_name + ".txt", "w") as f:
            for row in maze:
                for x in row:
                    f.write(str(x))
                f.write("\n")
    else:
        plt.show()


if __name__ == "__main__":
    main()
